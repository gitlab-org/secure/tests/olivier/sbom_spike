Extract PURLs from various output and check diff

```
cat gms-sbom.json| jq '.components.[]|.purl'|sort > gms

cat cdx-gradle-plugin-sbom.json| jq '.components.[]|.purl | sub("\\?type=jar";"")' |sort > cdx

diff gms cdx
```