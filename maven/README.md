Extract PURLs from various output and check diff

```
cat gms-sbom.json| jq '.components.[]|.purl'|sort > gms

cat mvn-dependency-tree-sbom.json| jq '.. | select(.artifactId?) |select(. != null) | "pkg:maven/\(.groupId)/\(.artifactId)@\(.version)"' | sort > maven

diff gms maven

cat cdx-maven-plugin-sbom.json| jq '.components.[]|.purl | sub("\\?type=jar";"")' |sort > cdx

diff gms cdx
```