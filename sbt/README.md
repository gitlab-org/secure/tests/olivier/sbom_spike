Extract PURLs from various output and check diff

```
cat gms-sbom.json| jq '.components.[]|.purl'|sort > gms

cat cdxgen-sbom.json| jq '.components.[]|.purl | sub("\\?type=jar";"")' |sort > cdxgen

diff gms cdxgen
```